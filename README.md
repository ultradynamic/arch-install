installation steps
==================

#### create installation media ####
 
>`sudo dd if=/path_to_arch_.iso of=/dev/sdX`

#### (optional) secure erase the disk ####

>`dd if=/dev/urandom of=/dev/HARDDRIVE`

#### parition the disk ####

>create 2 partitions, one for boot and one for the encrypted filesystem (use 200MB for boot partition, and create 1M BIOS boot parition for GPT disks (ef02)

>`fdisk/gdisk /dev/HARDDRIVE`

#### encrypt partitions ####

>`cryptsetup -c aes-xts-plain64 -s 512 -h sha512 -i 5000 -y luksFormat /dev/LVM_PARTITION`

>`cryptsetup luksDump /dev/LVM_PARTITION`

>`cryptsetup luksOpen /dev/LVM_PARTITION crypt`

#### create LVM pool ####

>`lvm pvcreate /dev/mapper/zzz_crypt`

>`lvm vgcreate lvmpool /dev/mapper/zzz_crypt`
    
>`lvm lvcreate -L 32GB -n root zzz_lvmpool`

>`lvm lvcreate -L 1GB -n swap zzz_lvmpool`

>`lvm lvcreate -l 100%FREE -n home zzz_lvmpool`

#### create and mount the file system ####

>`mkfs.ext4 /dev/BOOT_PARTITION`

>`mkfs.ext4 /dev/mapper/zzz_lvmpool-root`

>`mkfs.ext4 /dev/mapper/zzz_lvmpool-home`

>`mkswap /dev/mapper/zzz_lvmpool-swap`

>`swapon /dev/mapper/zzz_lvmpool-swap`

>`mount /dev/mapper/zzz_lvmpool-root /mnt`

>`mkdir /mnt/home`

>`mount /dev/mapper/zzz_lvmpool-home /mnt/home`

>`mkdir /mnt/boot`

>`mount /dev/BOOT_PARTITION /mnt/boot`

#### connect to wi-fi ####

>`wifi-menu`

#### install base system ####

>`pacstrap -i /mnt base base-devel`

#### generate fstab ####

>`genfstab -U -p /mnt >> /mnt/etc/fstab`

#### chroot into system ####
>`arch-chroot /mnt`

#### mkinitcpio ###

>`vim /etc/mkinitcpio.conf`

>edit the `HOOKS` variable (`encrypt` and `lvm2` are for FS encryption, `resume` is for enabling hibernate)

>`HOOKS="base udev autodetect modconf block encrypt lvm2 resume filesystems keyboard fsck"`

>`mkinitcpio -p linux`

#### install bootloader and update config ####

>`pacman -S grub`

>`vim /etc/default/grub`

>append `cryptdevice=UUID=zzzz-zzzz-1111-1111:crypt resume=dev/mapper/zzz_lvmpool-swap` to `GRUB_CMDLINE_LINUX`

>set grub defaults to include `loglevel=3` to reduce logging

>`grub-install --target=i386-pc --recheck /dev/HARDDRIVE`

>`grub-mkconfig -o /boot/grub/grub.cfg`

#### set locale, timezone, hostname ####

>`vim /etc/locale.gen`

>uncomment `en_US.UTF-8 UTF-8`

>`locale-gen`

>`echo LANG=en_US.UTF-8 > /etc/locale.conf`

>`export LANG=en_US.UTF-8`

>`ls /usr/share/zoneinfo/`

>`ln -s /usr/share/zoneinfo/<Zone>/<SubZone> /etc/localtime`

>`hwclock --systohc --utc`

> _note:_ if system dual boots with Windows and you can't change Windows to use UTC use this instead:

>`hwclock --hctosys --localtime`

>`echo my_host_name > /etc/hostname`

#### install some key utilities ####

>`pacman -S dialog wpa_supplicant bash-completion`

#### set root password and create first user ####

>`passwd`

>`useradd -m -g users -G wheel,storage,power,video,optical -s /bin/zsh my_user`

>`passwd my_user`

>`EDITOR=nano visudo`

>uncomment `%wheel ALL=(ALL) ALL`

#### unmount all and reboot ####

#### installing applications ####

>run install scripts to install all applications

>copy all config and dotfiles to home dir

application configuration
=========================

#### set up macchanger ####

>`vim /etc/systemd/system/macspoof@.service`

>>`[Unit]`
>>`Description=macchanger on %I`

>>`Wants=network-pre.target`

>>`Before=network-pre.target`

>>`BindsTo=sys-subsystem-net-devices-%i.device`

>>`After=sys-subsystem-net-devices-%i.device`

>>`[Service]`

>>`ExecStart=/usr/bin/macchanger -e %I`

>>`Type=oneshot`

>>`[Install]`

>>`WantedBy=multi-user.target`

>`sudo systemctl enable macspoof@wlpXsX.service`

#### change /etc/resolv.conf to use OpenNIC DNS servers ####

>`vim /etc/resolv.conf`

>>`nameserver 173.232.230.248`

>>`nameserver 50.116.40.226 `

>prevent DHCPD from overwriting the new servers:

>`vim /etc/dhcpd.conf`

>append:

>>`nohook resolv.conf`


#### set hostname and samba workgroup ####

>`sudo vim /etc/samba/smb.conf`

>append:

>>`workgroup = MY_WORKGROUP`

#### set-up timidity-freepats to enable MIDI ####

>`vim /etc/timidity++/timidity.cfg`

>append:

>>`dir /usr/share/timidity/freepats`
>>`source /etc/timidity++/freepats/freepats.cfg`

#### for IMGBurn / CDI burning in WINE ####

>`sudo modprobe sg`

